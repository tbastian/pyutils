import typing as t

import capstone
import pytest

from tbastian_pyutils import basic_blocks


def test_section_at():
    """Test the section_at method"""

    def new_sec(start: int, end: int) -> basic_blocks.Section:
        return basic_blocks.Section(
            name=f"s{start}",
            addr=start,
            size=end - start,
        )

    bbdec = basic_blocks.BBDecoder.new_bare()
    bbdec._sections = [
        s100 := new_sec(100, 200),
        s200 := new_sec(200, 300),
        s400 := new_sec(400, 1000),
    ]

    assert bbdec.section_at(100) == s100
    assert bbdec.section_at(150) == s100
    assert bbdec.section_at(199) == s100
    assert bbdec.section_at(200) == s200
    assert bbdec.section_at(400) == s400
    assert bbdec.section_at(999) == s400

    for val in [0, 99]:
        with pytest.raises(
            basic_blocks.SectionNotFound, match=r"No addressable.*too low.*"
        ):
            bbdec.section_at(val)

    for val in [300, 399, 1000, 1200]:
        with pytest.raises(
            basic_blocks.SectionNotFound, match=r"No addressable.*0x[0-9a-f]+$"
        ):
            bbdec.section_at(val)


def test_find_blocks_aarch64():
    """Test the _find_blocks method"""

    bbdec = basic_blocks.BBDecoder.new_bare()
    bbdec._cs = capstone.Cs(capstone.CS_ARCH_ARM64, capstone.CS_MODE_ARM)
    bbdec._cs.detail = True

    """
0x100
=====

   0:   d2800000        mov     x0, #0x0                        // #0
   4:   91000400        add     x0, x0, #0x1
   8:   f100281f        cmp     x0, #0xa
   c:   54ffffc1        b.ne    4
  10:   aa0003e1        mov     x1, x0
    """
    sym_to_bytes = {
        0x100: bytes.fromhex("0000 80d2 0004 0091 1f28 00f1 c1ff ff54 e103 00aa"),
    }
    syms = [
        basic_blocks.Symbol(name=f"s{addr:x}", addr=addr, size=len(insn))
        for addr, insn in sym_to_bytes.items()
    ]

    def feeder(sym: basic_blocks.Symbol) -> bytes:
        return sym_to_bytes[sym.addr]

    bbdec.elf_text_bytes = feeder

    assert bbdec.find_blocks(syms[0]) == [
        basic_blocks.Block(0x100, 4),
        basic_blocks.Block(0x104, 12),
        basic_blocks.Block(0x110, 4),
    ]
