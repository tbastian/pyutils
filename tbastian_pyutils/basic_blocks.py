""" Breaks down an ELF file into basic blocks """

import bisect
import logging
import typing as t
from dataclasses import dataclass
from pathlib import Path

import capstone
from elftools.elf import constants as elf_constants
from elftools.elf import sections
from elftools.elf.elffile import ELFFile
from sortedcontainers import SortedSet

logger = logging.getLogger(__name__)


class BasicBlockException(Exception):
    """Base class"""


class SectionNotFound(BasicBlockException):
    """Raised when the ELF file does not have an expected section"""


class SymbolsTableNotFound(SectionNotFound):
    """Raised when the ELF file does not have a symbols table"""


class BadAddress(BasicBlockException):
    """Raised when an address does not map correctly to an expected part of the elf
    file"""


class AddressInNonTextCode(BasicBlockException):
    """The address queried lies in the init/PLT"""


class AddressInNonCode(BasicBlockException):
    """The address queried is not in a code section"""


class SymbolNotFound(BasicBlockException):
    """Raised when a sample is not included in any symbol"""


class BlockNotFound(BasicBlockException):
    """Raised when a sample is not mappable to a block"""


class BadBlocks(BasicBlockException):
    """Raised when the blocks cannot be made to make sense"""


@dataclass(frozen=True)
class AddrRange:
    addr: int
    size: int

    @property
    def end(self):
        """Past-the-end address for this block"""
        return self.addr + self.size

    def __lt__(self, other):
        return self.addr < other.addr or (
            self.addr == other.addr and self.size > other.size
        )

    def __contains__(self, addr: int) -> bool:
        return self.addr <= addr < self.end

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(addr=0x{self.addr:x}, size={self.size})"


@dataclass(frozen=True)
class NamedRange(AddrRange):
    """Base class for a named address range"""

    name: str

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            + f"name={self.name}, addr=0x{self.addr:x}, size={self.size})"
        )


class Symbol(NamedRange):
    """An ELF symbol"""


class Section(NamedRange):
    """An ELF section"""


@dataclass(frozen=True)
class Block(AddrRange):
    """A basic block"""

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(addr=0x{self.addr:x}, size={self.size})"


class BlockData:
    """Basic block's content"""

    raw: bytes
    block: Block
    disasm: t.Optional[list[capstone.CsInsn]]

    _CONTROL_FLOW_GROUPS = set(
        [capstone.CS_GRP_JUMP, capstone.CS_GRP_RET, capstone.CS_GRP_CALL]
    )

    def __init__(self, raw: bytes, block: Block):
        self.raw = raw
        self.block = block
        self.disasm = None

    def do_disasm(self, cs: capstone.Cs):
        """Disassemble the raw bytes and fill the disasm field"""

        self.disasm = list(cs.disasm(self.raw, self.block.addr))

    def filter_control_insn(self) -> "BlockData":
        """Get a new basic block without the eventual final control flow instruction"""
        assert self.disasm is not None
        last_groups = set(self.disasm[-1].groups)
        if last_groups & self._CONTROL_FLOW_GROUPS:
            last_size = len(self.disasm[-1].bytes)
            out = self.__class__(
                raw=self.raw[:-last_size],
                block=Block(self.block.addr, self.block.size - last_size),
            )
            out.disasm = self.disasm[:-1]
            return out
        else:
            return self


class BBDecoder:
    elf_path: Path
    blocks: SortedSet[Block]

    _sections: list[Section]  # sorted by address
    _symbols: list[Symbol]  # sorted by address
    _processed_syms: set[Symbol]
    _elf_file: ELFFile
    _cs: capstone.Cs

    def __init__(self, elf: Path):
        self.elf_path = elf

        self.blocks = SortedSet(key=lambda x: x.addr)
        self._processed_syms = set()

        self._symbols = []
        self._sections = []
        self._elf_file = ELFFile(self.elf_path.open("rb"))
        self._init_symbols()
        self._init_sections()

        cs_arch, cs_mode = {
            "x86": (capstone.CS_ARCH_X86, capstone.CS_MODE_32),
            "x64": (capstone.CS_ARCH_X86, capstone.CS_MODE_64),
            "ARM": (capstone.CS_ARCH_ARM, capstone.CS_MODE_ARM),
            "AArch64": (capstone.CS_ARCH_ARM64, capstone.CS_MODE_ARM),
        }[self._elf_file.get_machine_arch()]
        logger.debug(
            "ELF arch %s: using Capstone with arch %d, mode %d",
            self._elf_file.get_machine_arch(),
            cs_arch,
            cs_mode,
        )
        self._cs = capstone.Cs(cs_arch, cs_mode)
        self._cs.detail = True
        self._cs.syntax = capstone.CS_OPT_SYNTAX_ATT
        # Computing the full CFG is just too long -- we'd rather compute CFGs local to
        # symbols.

    @classmethod
    def new_bare(cls):
        """Create a new bare instance, without an ELF. The caller is responsible for
        initializing class fields. For unit tests."""
        return cls.__new__(cls)

    @classmethod
    def _mk_section(cls, sec: sections.Section) -> Section:
        return Section(addr=sec.header.sh_addr, size=sec.header.sh_size, name=sec.name)

    def _init_sections(self):
        """Loads sections from the ELF file"""
        for sec in self._elf_file.iter_sections():
            if (
                sec.header.sh_size == 0
                or sec.header.sh_flags & elf_constants.SH_FLAGS.SHF_ALLOC == 0
            ):  # Section is not mapped in memory
                continue
            self._sections.append(self._mk_section(sec))
        self._sections.sort(key=lambda sec: sec.addr)

    def _init_symbols(self):
        """Loads symbols from the ELF file"""
        symtab = self._elf_file.get_section_by_name(".symtab")
        if symtab is None:
            raise SymbolsTableNotFound(f"No .symtab section in {self.elf_path}")

        raw_symbols: list[sections.Symbol] = list(
            filter(lambda x: x.entry.st_size > 0, symtab.iter_symbols())
        )

        self._symbols = [
            Symbol(name=sym.name, addr=sym.entry.st_value, size=sym.entry.st_size)
            for sym in raw_symbols
        ]
        self._symbols.sort(key=lambda x: x.addr)

    def section_at(self, addr: int) -> Section:
        """Get the ELF section at this address"""
        base_addr_index = bisect.bisect_right(
            self._sections, Section(addr=addr, size=0, name="")
        )
        if base_addr_index == 0:
            raise SectionNotFound(
                f"No addressable section at address 0x{addr:x} (too low)"
            )
        sec: Section = self._sections[base_addr_index - 1]
        if addr not in sec:
            raise SectionNotFound(f"No addressable section at address 0x{addr:x}")
        return sec

    def get_section(self, name: str) -> Section:
        """Get the ELF section with this name"""
        section = self._elf_file.get_section_by_name(name)
        if section is None:
            raise SectionNotFound(name)
        return self._mk_section(section)

    def _symbol_id_at(self, addr: int) -> int:
        section: str = self.section_at(addr).name
        if section != ".text":
            if section in [".plt", ".plt.got", ".init"]:
                raise AddressInNonTextCode(f"Address 0x{addr:x} is in {section}")
            raise AddressInNonCode(f"Address 0x{addr:x} is in non-code {section}")

        base_addr_index = bisect.bisect_right(
            self._symbols, Symbol(addr=addr, size=0, name="")
        )
        if base_addr_index == 0:
            raise SymbolNotFound(
                f"Could not find a symbol containing address {addr:x} "
                + "(address is too low)"
            )
        return base_addr_index - 1

    def symbol_at(self, addr: int) -> Symbol:
        """Find the symbol containing this address"""
        base_addr_index = self._symbol_id_at(addr)
        sym: Symbol = self._symbols[base_addr_index]
        if sym.end <= addr:
            raise SymbolNotFound(f"Could not find a symbol containing address {addr:x}")
        return sym

    def symbols_in(self, addr_range: AddrRange) -> t.Generator[Symbol, None, None]:
        """Lists the symbols intersecting this address range"""
        first_sym_id = self._symbol_id_at(addr_range.addr)
        last_sym_id = self._symbol_id_at(addr_range.end - 1)

        first_sym = self._symbols[first_sym_id]
        if first_sym.end <= addr_range.addr:
            first_sym_id += 1

        for sym_id in range(first_sym_id, last_sym_id + 1):
            yield self._symbols[sym_id]

    def get_symbol(self, symbol_name: str) -> Symbol:
        """Find the symbol with this name, or raises SymbolNotFound"""
        for symbol in self._symbols:
            if symbol.name == symbol_name:
                return symbol
        raise SymbolNotFound(symbol_name)

    def elf_text_bytes(self, location: AddrRange) -> bytes:
        """Get a range of bytes from the .text section of the elf file"""

        text = self._elf_file.get_section_by_name(".text")
        if text is None:
            raise SectionNotFound(".text")

        offset = text.header.sh_offset
        text_range = AddrRange(addr=offset, size=text.header.sh_size)
        if location.addr not in text_range or location.end - 1 not in text_range:
            raise BadAddress(
                f"Cannot read {location.size} bytes at address {location.addr:x} "
                + "from .text: out of section"
            )

        return text.data()[location.addr - offset : location.end - offset]

    def get_block_data(self, block: Block, disasm: bool = False) -> BlockData:
        """Extract a BlockData from the ELF file. If disasm is true, use Capstone to
        disassemble it."""
        out = BlockData(self.elf_text_bytes(block), block)
        if disasm:
            out.do_disasm(self._cs)
        return out

    def find_blocks(self, sym: Symbol) -> list[Block]:
        """Disassemble the symbol and split it into basic blocks"""
        insn_bytes = self.elf_text_bytes(sym)
        insn_addresses: set[int] = set()
        disasm = self._cs.disasm(insn_bytes, sym.addr)
        blocks: list[Block] = []
        jumpsites: SortedSet[int] = SortedSet()

        cur_begin = sym.addr
        insn: capstone.CsInsn
        for insn in disasm:
            insn_addresses.add(insn.address)
            if any(
                (
                    grp in insn.groups
                    for grp in [
                        capstone.CS_GRP_JUMP,
                        capstone.CS_GRP_CALL,
                        capstone.CS_GRP_RET,
                    ]
                )
            ):
                # Instruction is a block boundary
                next_addr = insn.address + insn.size
                blocks.append(Block(addr=cur_begin, size=next_addr - cur_begin))
                cur_begin = next_addr

                # Find jumpsites
                if capstone.CS_GRP_JUMP in insn.groups:
                    # If this instruction has a single immediate operand, whose value
                    # is inside this symbol's address range, then we can guess it is a
                    # jump site.
                    if (
                        len(insn.operands) == 1
                        and insn.operands[0].type == capstone.CS_OP_IMM
                    ):
                        jump_addr = insn.operands[0].imm
                        if jump_addr in sym:
                            jumpsites.add(jump_addr)

        # Last block
        end_addr = insn.address + insn.size
        if cur_begin != end_addr:
            blocks.append(Block(addr=cur_begin, size=end_addr - cur_begin))

        # Split blocks on jump sites
        split_blocks: list[Block] = []
        for block in blocks:
            first_jumpsite = jumpsites.bisect_left(block.addr)
            last_jumpsite = jumpsites.bisect_right(block.end)
            cur_start = block.addr

            for jumpsite in jumpsites[first_jumpsite:last_jumpsite]:
                if jumpsite not in insn_addresses:  # Bad jumpsite
                    continue
                block_size = jumpsite - cur_start
                if block_size == 0:
                    continue
                split_blocks.append(Block(addr=cur_start, size=block_size))
                cur_start = jumpsite
            if cur_start < block.end:
                split_blocks.append(Block(addr=cur_start, size=block.end - cur_start))

        return split_blocks

    def block_at(self, addr: int) -> Block:
        """Find which block this address is included in. If this block is not yet
        discovered, will perform a CFG analysis on this address' symbol."""

        sym = self.symbol_at(addr)

        if sym not in self._processed_syms:
            logger.debug("Block at 0x%x: symbol %s not processed yet", addr, sym.name)
            blocks: list[Block] = self.find_blocks(sym)
            for block in blocks:
                self.blocks.add(block)
            self._processed_syms.add(sym)

        cur_block_id = self.blocks.bisect_right(Block(addr=addr, size=0))
        if cur_block_id == 0:
            raise BlockNotFound(f"No basic block found at address {addr:x}")
        cur_block = self.blocks[cur_block_id - 1]
        logger.debug("Found block <%x:%d>", cur_block.addr, cur_block.size)
        if addr not in cur_block:
            raise BlockNotFound(f"No basic block found at address {addr:x}")
        return cur_block
