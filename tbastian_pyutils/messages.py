import datetime
import logging
import typing as t
from pathlib import Path

_CEND = "\33[0m"
_CBOLD = "\33[1m"
_CITALIC = "\33[3m"
_CURL = "\33[4m"
_CBLINK = "\33[5m"
_CBLINK2 = "\33[6m"
_CSELECTED = "\33[7m"

_CBLACK = "\33[30m"
_CRED = "\33[31m"
_CGREEN = "\33[32m"
_CYELLOW = "\33[33m"
_CBLUE = "\33[34m"
_CVIOLET = "\33[35m"
_CBEIGE = "\33[36m"
_CWHITE = "\33[37m"

_CBLACKBG = "\33[40m"
_CREDBG = "\33[41m"
_CGREENBG = "\33[42m"
_CYELLOWBG = "\33[43m"
_CBLUEBG = "\33[44m"
_CVIOLETBG = "\33[45m"
_CBEIGEBG = "\33[46m"
_CWHITEBG = "\33[47m"

_CGREY = "\33[90m"
_CRED2 = "\33[91m"
_CGREEN2 = "\33[92m"
_CYELLOW2 = "\33[93m"
_CBLUE2 = "\33[94m"
_CVIOLET2 = "\33[95m"
_CBEIGE2 = "\33[96m"
_CWHITE2 = "\33[97m"

_CGREYBG = "\33[100m"
_CREDBG2 = "\33[101m"
_CGREENBG2 = "\33[102m"
_CYELLOWBG2 = "\33[103m"
_CBLUEBG2 = "\33[104m"
_CVIOLETBG2 = "\33[105m"
_CBEIGEBG2 = "\33[106m"
_CWHITEBG2 = "\33[107m"


class LoggingCustomFormatter(logging.Formatter):
    """Logging Formatter to add colours"""

    log_format = "[{time}|%(levelname)s] (%(name)s) — %(message)s"

    FORMATS = {
        logging.DEBUG: _CBLUE2 + log_format + _CEND,
        logging.INFO: _CGREEN + log_format + _CEND,
        logging.WARNING: _CYELLOW2 + log_format + _CEND,
        logging.ERROR: _CRED2 + log_format + _CEND,
        logging.CRITICAL: _CRED2 + _CBOLD + log_format + _CEND,
    }

    def __init__(self, relative_time: bool):
        self._starttime = datetime.datetime.now()
        self._relative_time = relative_time

    def format(self, record):
        time: datetime.timedelta | datetime.datetime
        if self._relative_time:
            time = datetime.timedelta(
                seconds=int((datetime.datetime.now() - self._starttime).total_seconds())
            )
        else:
            time = datetime.datetime.now()
        log_fmt = self.FORMATS[record.levelno].format(time=time)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def setup_logging(
    loglevel=logging.INFO, log_tee: t.Optional[Path] = None, relative_time: bool = True
):
    """Configure Python's logging module.

    If `log_tee` is not None, also logs everything at this path. If the file already
    exists, rotates it.
    If `relative_time` is True (default), the time displayed in each logline will be
    relative to the program's start time.
    """
    stdout_handler = logging.StreamHandler()
    stdout_handler.setFormatter(LoggingCustomFormatter(relative_time))
    handlers: list[logging.Handler] = [stdout_handler]

    if log_tee is not None:
        file_handler = logging.FileHandler(log_tee, mode="w")
        file_handler.setFormatter(LoggingCustomFormatter(relative_time=relative_time))
        handlers.append(file_handler)

    logging.basicConfig(level=loglevel, handlers=handlers)
