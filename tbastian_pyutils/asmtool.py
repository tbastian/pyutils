import enum
import re

import capstone


def get_x86_64_capstone() -> capstone.Cs:
    cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)
    cs.detail = True
    return cs


class SimdStatus(enum.Enum):
    Unknown = enum.auto()
    Simd = enum.auto()
    NoSimd = enum.auto()


_SIMD_GROUPS = set(
    [
        capstone.x86_const.X86_GRP_AVX,
        capstone.x86_const.X86_GRP_AVX2,
        capstone.x86_const.X86_GRP_AVX512,
        capstone.x86_const.X86_GRP_SSE1,
        capstone.x86_const.X86_GRP_SSE2,
        capstone.x86_const.X86_GRP_SSE3,
        capstone.x86_const.X86_GRP_SSE41,
        capstone.x86_const.X86_GRP_SSE42,
        capstone.x86_const.X86_GRP_SSE4A,
    ]
)
_SIMD_MNEMONIC_RE = re.compile(r".*(?P<packed>[PS])(?P<typ>[SD])$")


def heuristic_is_simd(insn: capstone.CsInsn) -> SimdStatus:
    """Try to figure out whether an x86 instruction is simd"""
    if insn.operands:
        assert isinstance(insn.operands[0], capstone.x86.X86Op)

    if not (set(insn.groups) & _SIMD_GROUPS):
        return SimdStatus.NoSimd

    if match := _SIMD_MNEMONIC_RE.fullmatch(insn.mnemonic.upper()):
        if match["packed"] == "P":
            return SimdStatus.Simd
        return SimdStatus.NoSimd

    if insn.mnemonic.upper() in ["PXOR"]:
        return SimdStatus.NoSimd

    return SimdStatus.Unknown
