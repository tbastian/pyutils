#!/usr/bin/env python3

from setuptools import setup, find_packages
import sys


def parse_requirements():
    reqs = []
    with open("requirements.txt", "r") as handle:
        for line in handle:
            if line.startswith("-") or line.startswith("git+"):
                continue
            reqs.append(line)
    return reqs


setup(
    name="tbastian_pyutils",
    version="0.0.4",
    description="Various snippets used throughout my code",
    author="CORSE",
    license="LICENSE",
    packages=find_packages(),
    include_package_data=True,
    package_data={"tbastian_pyutils": ["py.typed"]},
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={},
)
